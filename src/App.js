import React, {useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import { UserProvider } from "./userContext";
import Landing from "./components/Landing"
import Signup from "./pages/signup"
import Login from "./pages/login"
import Shop from "./pages/shop"
import Products from "./pages/products";
import AddProduct from "./pages/addProduct"
import CheckOut from "./pages/cart";
import ShippingDetails from "./pages/shippingDetails"
import ViewOrder from "./pages/orders";



function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true"
  })

  function unsetUser() {
    localStorage.clear()
  }


  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/shop" component={Shop} />
          <Route exact path="/products" component={Products} />
          <Route exact path="/products/add" component={AddProduct} />
          <Route exact path="/cart" component={CheckOut} />
          <Route exact path="/shipping-details" component={ShippingDetails} />
          <Route exact path="/orders" component={ViewOrder} />
        </Switch>
      </Router>
    </UserProvider>
  );
}

export default App;
