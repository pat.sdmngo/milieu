import React from 'react'
import { Container, Jumbotron, Row, Col } from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { NavLink } from "react-router-dom";
import "../css/landing.css";
import livingroom from "../img/living-room-gradient-2.jpg"


export default function Landing() {
  return (
    <Jumbotron
      style={{
        backgroundImage: `url(${livingroom})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
      fluid
    >
    <div className="overlay"></div>
      <div className="landing-nav">
        <Navbar>
          <Navbar.Brand className="landing-brand" href="#home">
            Milieu
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-end"
          >
            <Nav.Link
              className="landing-links"
              as={NavLink}
              to="/"
              href="#link"
            >
              About
            </Nav.Link>
            <Nav.Link className="landing-links" as={NavLink} to="/signup">
              Signup
            </Nav.Link>
            <Nav.Link className="landing-links" as={NavLink} to="/login">
              Login
            </Nav.Link>
            <Nav.Link className="landing-links" as={NavLink} to="/shop">
              Shop Now
            </Nav.Link>
          </Navbar.Collapse>
        </Navbar>
      </div>
      <h1 className="catch-phrase">Design your life.</h1>
      <Container fluid>
        <Row className="motto-whole">
          <Col lg={4} md={4} sm={12} className="motto-container">
            <h4 className=" motto landing-motto">Only the Best</h4>
            <p className="motto landing-motto-text">
              All our products are filtered down from a wide range of great
              brands both local and international to only give you the best
              option in creating your dream home.
            </p>
          </Col>
          <Col lg={4} md={4} sm={12} className="motto-container">
            <h4 className="motto landing-motto">Handpicked by Professionals</h4>
            <p className="motto landing-motto-text">
              Each of our items were selected and endorsed by our partners in
              the industry, designers and architects alike, that ensures you
              top-quality-specifications.
            </p>
          </Col>
          <Col lg={4} md={4} sm={12} className="motto-container">
            <h4 className="motto landing-motto">Design your Dream</h4>
            <p className="motto landing-motto-text">
              Whatever mood or vibe you are aiming for, you can find the perfect
              pieces to fit in here. Start browsing now and find furnitures that
              will bring color and life to your home!
            </p>
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  );
} 