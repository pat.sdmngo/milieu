import React from 'react'
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import "../css/footer.css"


export default function Footer() {
  return (
    <>
      <div className="footer-component">
        <Container>
          <Row>
            <Col>
              <h2 className="footer-newsletter">
                Subscribe to get our latest updates
              </h2>
            </Col>
            <Col>
              <Row>
                <Col className="pr-0">
                  <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Please enter your email"
                    />
                    <Form.Text className="text-muted">
                      We promise we won't spam you with unimportant emails.
                    </Form.Text>
                  </Form.Group>
                </Col>
                <Col lg={3} className="pl-0">
                  <Button
                    className="signup-btn footer-btn"
                    variant="dark"
                    type="submit"
                  >
                    Sign Up
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <hr />
          <Row className="icons holder">
            <Col>
              <h3 className="footer-brand">Milieu</h3>
              <div className="footer-icons">
                <a href="https://www.facebook.com/" class="fa fa-facebook"></a>
                <a href="https://www.twitter.com/" class="fa fa-twitter"></a>
                <a href="https://www.instagram.com/" class="fa fa-instagram"></a>
                <a href="https://www.pinterest.ca/" class="fa fa-pinterest"></a>
              </div>
            </Col>
            <Col>
              <h5 className="footer-nav">Navigation</h5>
              <p className="footer-text">About</p>
              <p className="footer-text">Portfolio</p>
              <p className="footer-text">Sign Up</p>
              <p className="footer-text">Login</p>
              <p className="footer-text">Shop Now</p>
            </Col>
            <Col>
              <h5 className="footer-nav">Contact</h5>
              <p className="footer-text">Landline: (02) 8765-2331</p>
              <p className="footer-text">Trunkline: (02) 8765-5677 - 80</p>
              <p className="footer-text">Mobile: 0956-835-9887</p>
              <p className="footer-text">Email: welcome@milieu.com</p>
            </Col>
            <Col>
              <h5 className="footer-nav">Address</h5>
              <p className="footer-text">
                1600 Amphitheatre Parkway in Mountain View, California 94043,
                United States
              </p>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <div className="footer-closing">
              <p className="footer-end">
                &#169; 2021 The Furniture Store Group of Companies
              </p>
              <p className="footer-last">
                Designed by Architrix Design Studios
              </p>
            </div>
          </Row>
        </Container>
      </div>
    </>
  );
}