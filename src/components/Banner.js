import React from "react";
import { Carousel } from "react-bootstrap";
import carousel1 from "../img/carousel-1.jpg";
import carousel2 from "../img/carousel-2.jpg";
import carousel3 from "../img/carousel-3.jpg";
import carousel4 from "../img/carousel-4.jpg";
import "../css/banner.css"



export default function Banner() {
  return (
    <Carousel>
      <Carousel.Item>
        <img className="d-block w-100" src={carousel1} alt="First slide" />
        <Carousel.Caption>
          <h3 className="banner-label">Check out the weekly Top Picks!</h3>
          <p className="banner-text">We collected the Top 3 most viewed products for you to choose from. Shop this look now while stocks last!</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={carousel2} alt="Second slide" />
        <Carousel.Caption>
          <h3 className="banner-label">Simple and Sleek</h3>
          <p className="banner-text">Perfect for adding a touch of color to your industrial design vibes.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={carousel3} alt="Third slide" />
        <Carousel.Caption>
          <h3 className="banner-label">Comfy Home</h3>
          <p className="banner-text">
            Share a pleasant dinner time with your family in this homey dining set.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={carousel4} alt="Third slide" />
        <Carousel.Caption>
          <h3 className="banner-label">Calm and Relaxing</h3>
          <p className="banner-text">
            When it comes to comfort, green and neutrals will never fade out of style.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}
