import React, {useContext} from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { NavLink } from "react-router-dom"
import Swal from "sweetalert2";
import UserContext from "../userContext"
import '../css/navbar.css'



export default function NavBar() {

  const {user, setUser, unsetUser} = useContext(UserContext)

  function logOut() {
    unsetUser()
    setUser({
      email: null,
      isAdmin: null
    })
    
    Swal.fire({
      icon: "success",
      title: "Logout Successful.",
      text: `See you again!`
    });

    window.location.replace("/login");
  }

    return (
      <Navbar className="nav-black" expand="lg">
        <Navbar.Brand id="generic-nav" as={NavLink} to="/" href="#">
          Milieu
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav className="mr-auto" id="nav-set">
            {
              user.email
              ?
                user.isAdmin
                ?
                <>
                    
                  <Nav.Link id="link-all" as={NavLink} to="/products">DashBoard</Nav.Link>
                  <Nav.Link id="link-all" as={NavLink} to="/products/add">Add Product</Nav.Link>
                  <Nav.Link id="link-all" onClick={logOut}>Logout</Nav.Link>
                </>
                :
                <>
                  <Nav.Link id="link-all" as={NavLink} to="/cart">Cart</Nav.Link>
                  <Nav.Link id="link-all" as={NavLink} to="/orders">Orders</Nav.Link>
                  <Nav.Link id="link-all" onClick={logOut}>Logout</Nav.Link>
                </>
              :
              <>
                <Nav.Link id="link-all" as={NavLink} to="/cart">Cart</Nav.Link>
								<Nav.Link id="link-all" as={NavLink} to="/signup">Signup</Nav.Link>
								<Nav.Link id="link-all" as={NavLink} to="/login">Login</Nav.Link>
							</>
            }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
}