import React, {useEffect, useState, useContext} from 'react'
import {Redirect} from 'react-router-dom'
import UserContext from "../userContext"
import { Card, Table, Button } from "react-bootstrap";
import "../css/product.css";
import placeholder from "../img/placeholder-img.jpg"
import Swal from "sweetalert2";



export default function Product({prodProp}) {

  const [cart, setCart] = useState([])
  const {user, setUser} = useContext(UserContext)


  function addToCart(prodProp) {
    console.log(prodProp)
    fetch("https://pure-caverns-71461.herokuapp.com/api/orders", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        user: user,
        product: prodProp._id,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCart(data);
      });

    Swal.fire({
      position: "top-end",
      icon: "success",
      title: "Item added to the cart!",
      showConfirmButton: false,
      timer: 1500,
    });
  }





  let price = prodProp.price
  let getPrice = price.toLocaleString(undefined, {minimumFractionDigits: 2})
    return (
      <Card className="border-light card-design product-card">
        <Card.Body className="pb-0">
          <Card.Img
            src={placeholder}
            alt="Furniture placeholder"
            fluid="true"
          />
          <Card.Title className="product-title">{prodProp.name}</Card.Title>
          <Card.Subtitle className="product-subtitle">
            {prodProp.collectionName}
          </Card.Subtitle>
        </Card.Body>
        <Card.Body className="pt-0">
          <Card.Text>Product Description:</Card.Text>
          <Table bordered size="sm" responsive>
            <tbody>
              <tr>
                <td className="table-data-bold">Type</td>
                <td className="table-data-reg">
                  {prodProp.description.productType}
                </td>
              </tr>
              <tr>
                <td className="table-data-bold">Color</td>
                <td className="table-data-reg">{prodProp.description.color}</td>
              </tr>
              <tr>
                <td className="table-data-bold">Size</td>
                <td className="table-data-reg">{prodProp.description.size}</td>
              </tr>
            </tbody>
          </Table>
          <Card.Text id="price">PHP {getPrice}</Card.Text>
          <div>
            <Button
              variant="outline-dark"
              className="cart-button"
              onClick={() => addToCart(prodProp)}
            >
              Add to Cart
            </Button>
          </div>
        </Card.Body>
      </Card>
    );
}