import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'


export default function Order({orderProp}) {


  let orderTotal = orderProp.orderTotal
  let grandTotal = orderTotal.toLocaleString(undefined, {
    minimumFractionDigits: 2,
  });

  let totalAmount = orderProp.totalAmount;
  let subtotal = totalAmount.toLocaleString(undefined, {
    minimumFractionDigits: 2,
  });

  let shippingFee = orderProp.shippingFee
  let shipping = shippingFee.toLocaleString(undefined, {
    minimumFractionDigits: 2,
  });

  return (
    <>
      <Container className="order-card">
        <Row>
          <Col lg={4}>
            <p className="order-sub">Purchased On:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">{orderProp.purchasedOn}</p>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <p className="order-sub">Delivery Date:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">{orderProp.deliveryDate}</p>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <p className="order-sub">Shipping Address:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">{orderProp.shippingAddress}</p>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <p className="order-sub">Quantity:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">{orderProp.cart.length}</p>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <p className="order-sub">Subtotal:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">PHP {subtotal}</p>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <p className="order-sub">Shipping Fee:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">PHP {shipping}</p>
          </Col>
        </Row>
        <Row>
          <Col lg={4}>
            <p className="order-sub">Grand Total:</p>
          </Col>
          <Col lg={8}>
            <p className="order-text">PHP {grandTotal}</p>
          </Col>
        </Row>
      </Container>
    </>
  );
}