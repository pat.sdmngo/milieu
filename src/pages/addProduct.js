import React, { useState, useEffect, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import { Redirect } from "react-router-dom";
import UserContext from "../userContext";
import NavBar from "../components/NavBar"
import Footer from "../components/Footer"


export default function AddProduct() {

  const [name, setName] = useState("");
  const [productType, setProductType] = useState("");
  const [color, setColor] = useState("");
  const [size, setSize] = useState("");
  const [collectionName, setCollectionName] = useState("");
  const [price, setPrice] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [willRedirect, setWillRedirect] = useState(false);
  const { user, setUser } = useContext(UserContext);


  useEffect(() => {
    if (name !== "" && productType !== "" && color !== "" && size !== "" && collectionName !== "" && price !== "") {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [name, productType, color, size, collectionName, price])

  function handleName(event) {
    setName(event.target.value)
  }

  function handleProductType(event) {
    setProductType(event.target.value);
  }

  function handleColor(event) {
    setColor(event.target.value);
  }

  function handleSize(event) {
    setSize(event.target.value);
  }

  function handleCollectionName(event) {
    setCollectionName(event.target.value);
  }

  function handlePrice(event) {
    setPrice(event.target.value);
  }

  function newProduct(event) {
    event.preventDefault()

    fetch("https://pure-caverns-71461.herokuapp.com/api/products", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: name,
        description: {
          productType: productType,
          color: color,
          size: size
        },
        price: price,
        collectionName: collectionName
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data.message) {
        Swal.fire({
          icon: "error",
          title: "Failed to add product.",
          text: data.message
        })
      } else {
        Swal.fire({
          icon: "success",
          title: "Product added successfully!",
          text: "Go to the products page to view all products."
        })
      }
    })
    setName("")
    setProductType("")
    setColor("")
    setSize("")
    setCollectionName("")
    setPrice("")
  }



  return (
    !user.isAdmin || !user.email
    ?
    <Redirect to="/login"/>
    :
    <>
      <NavBar />
      <Container className="add-product-card">
        <Form onSubmit={(e) => newProduct(e)}>
          <Form.Group>
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Name"
              value={name}
              onChange={handleName}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Collection Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Collection Name"
              value={collectionName}
              onChange={handleCollectionName}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Price"
              value={price}
              onChange={handlePrice}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Type</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Type"
              value={productType}
              onChange={handleProductType}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Color</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Color"
              value={color}
              onChange={handleColor}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Size</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Product Size"
              value={size}
              onChange={handleSize}
            ></Form.Control>
          </Form.Group>
          {isActive ? (
            <Button className="signup-btn" variant="secondary" type="submit">
              Add Product
            </Button>
          ) : (
            <Button
              className="signup-btn"
              disabled
              variant="secondary"
              type="submit"
            >
              Add Product
            </Button>
          )}
        </Form>
      </Container>
      <Footer/>
    </>
  );
}