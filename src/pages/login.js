import React, { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Row, Col, Image } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../userContext";
import NavBar from "../components/NavBar";
import { Redirect } from "react-router-dom";
import login from "../img/login.jpg";
import "../css/signup.css"
import Footer from "../components/Footer"


export default function Login() {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    const {user, setUser} = useContext(UserContext)
    const [willRedirect, setWillRedirect] = useState(false)

    useEffect(() => {
      if (email !== "" && password !== "") {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    });

    function handleEmail(event) {
        setEmail(event.target.value)
    } 

    function handlePassword(event) {
      setPassword(event.target.value);
    }

    function loginUser(event) {
        event.preventDefault()
        fetch("https://pure-caverns-71461.herokuapp.com/api/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: email,
            password: password,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data.message) {
              Swal.fire({
                icon: "error",
                title: "Login Failed.",
                text: data.message,
              });
            } else {
              localStorage.setItem("token", data.accessToken);
              fetch("https://pure-caverns-71461.herokuapp.com/api/profile", {
                headers: {
                  Authorization: `Bearer ${data.accessToken}`,
                },
              })
                .then((res) => res.json())
                .then((data) => {
                  console.log(data);
                  localStorage.setItem("email", data.email);
                  localStorage.setItem("isAdmin", data.isAdmin);

                  setUser({
                    email: data.email,
                    isAdmin: data.isAdmin,
                  });

                  setWillRedirect(true);

                  Swal.fire({
                    icon: "success",
                    title: "You have logged in successfully!",
                    test: `Welcome back, ${data.firstName}!`,
                  });
                });
            }
          });

        setEmail("")
        setPassword("");
    }


    return user.email || willRedirect ? (
      <Redirect to="/shop" />
    ) : (
      <>
        <NavBar/>
        <Container className="login-card">
          <Row>
            <Col lg={6} sm={12}>
              <Form onSubmit={loginUser} className="pt-5 pl-4 pr-3">
                <Form.Group>
                  <Form.Label>Email:</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="email"
                    placeholder="Enter Email"
                    value={email}
                    onChange={handleEmail}
                  ></Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Password:</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="password"
                    placeholder="Enter Password"
                    value={password}
                    onChange={handlePassword}
                  ></Form.Control>
                </Form.Group>
                <Row>
                  <Col>
                    {isActive ? (
                      <Button
                        className="signup-btn"
                        variant="dark"
                        type="submit"
                      >
                        Submit
                      </Button>
                    ) : (
                      <Button
                        className="signup-btn"
                        disabled
                        variant="dark"
                        type="submit"
                      >
                        Submit
                      </Button>
                    )}
                  </Col>
                  <Col>
                    <p className="form-parag">
                      Don't have an account yet? Register <a href="/signup">here</a>
                    </p>
                  </Col>
                </Row>
              </Form>
            </Col>
            <Col lg={6} sm={12} className="p-0 order-first">
              <Image src={login} fluid />
            </Col>
          </Row>
        </Container>
        <Footer/>
      </>
    );
}