import React, {useState, useContext, useEffect} from 'react'
import { Redirect } from "react-router-dom";
import UserContext from "../userContext";
import { Container, Row, Col, Button, Image } from "react-bootstrap";
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import "../css/product.css"
import { v4 as uuidv4 } from 'uuid'
import placeholder from "../img/placeholder-img.jpg";


export default function CheckOut() {
  const [cart, setCart] = useState([]);
  const {user, setUser} = useContext(UserContext);


  useEffect(() => {
    fetch("https://pure-caverns-71461.herokuapp.com/api/orders/cart", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCart(data);
      });
  }, []);

  let cartArray = cart.map((cart) => {
    console.log(cart.status)
    return cart.products.map(product => {
      return {...product, cartId: cart._id, status: cart.status}
    })
  });

  let reduceArray = cartArray.reduce((initial, current) => {
    return [...initial, ...current];
  }, []);

  let unnestArray = reduceArray.filter(item => {
    return item.status === "Pending"
  })

  let calculate = unnestArray.map((product) => {
    return product.description.quantity * product.price;
  });

  let totalAmount = calculate.reduce((initial, current) => {
    return initial + current;
  }, 0);

  let displayAmount = totalAmount.toLocaleString(undefined, {
    minimumFractionDigits: 2,
  });


  function deleteItem(cartId) {
    fetch("https://pure-caverns-71461.herokuapp.com/api/orders/cart", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        cartId: cartId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCart(data);
      });
  }

  console.log(unnestArray)


  let cartDetails = unnestArray.map((cartItem) => {
    let price = cartItem.price;
    let getPrice = price.toLocaleString(undefined, {
      minimumFractionDigits: 2,
    });
    return (
      <Container className="cart-details" key={uuidv4()}>
        <Row>
          <Col lg={2}>
            <Image src={placeholder} fluid />
          </Col>
          <Col lg={5}>
            <p className="cart-title">{cartItem.name}</p>
            <p className="cart-sub">
              {cartItem.description.color} • {cartItem.description.size}
            </p>
          </Col>
          <Col lg={1}>
            <p className="cart-sub">{cartItem.description.quantity}</p>
          </Col>
          <Col lg={2}>
            <p className="cart-price">PHP {getPrice}</p>
          </Col>
          <Col lg={2} className="cart-btn-holder">
            <Button
              onClick={() => deleteItem(cartItem.cartId)}
              variant="outline-dark"
              className="cart-btn">
              Remove
            </Button>
          </Col>
        </Row>
      </Container>
    );
  });



  return (
    !user.email
    ?
    <Redirect to="/login"/>
    :
    <>
      <NavBar />
      {unnestArray.length === 0 ? (
        <div className="cart-holder">
          <h1 className="empty-cart">There are no items on your cart.</h1>
        </div>
      ) : (
        <div className="cart-holder">{cartDetails}</div>
      )}
      <Container className="cart-order-total">
        <Row>
          <Col>
            <h4 className="cart-total">Total: PHP {displayAmount}</h4>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button
              href="/shop"
              className="checkout-btn"
              variant="outline-dark"
            >
              Go Back To Shop
            </Button>
          </Col>
          <Col>
            {
              cart.length === 0
              ?
              <Button disabled href="/shipping-details" className="checkout-btn" variant="danger">
              Proceed to Checkout
              </Button>
              :
              <Button href="/shipping-details" className="checkout-btn" variant="danger">
              Proceed to Checkout
              </Button>
            }
          </Col>
        </Row>
      </Container>
      <Footer />
    </>
  );
}