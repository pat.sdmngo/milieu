import React, {useState, useEffect} from 'react'
import Order from "../components/Order"
import NavBar from "../components/NavBar"
import Footer from "../components/Footer"
import {Button, Container, Row} from 'react-bootstrap'


export default function ViewOrder() {

  const [order, setOrder] = useState([])

  useEffect(() => {
    fetch("https://pure-caverns-71461.herokuapp.com/api/order-details", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrder(data);
      });
  }, [])

  console.log(order)

  let orderArray = order.map(order => {
    return (
      <Row>
        <Order orderProp={order} />
      </Row>
    );
  })



  return order.length === 0 ? (
    <>
      <NavBar />
      <div className="cart-holder">
        <h1 className="empty-cart">You have no orders yet.</h1>
      </div>
      <Footer/>
    </>
  ) : (
    <>
      <NavBar />
      <Container className="order-holder">{orderArray}</Container>
      <div className="button-holder">
        <Button className="back-btn" href="/shop" variant="outline-dark">
          Go Back to Shop
        </Button>
      </div>
      <Footer />
    </>
  );
}