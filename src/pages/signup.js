import React, { useState, useEffect } from "react";
import { Form, Button, Container, Row, Col, Image } from "react-bootstrap";
import Swal from "sweetalert2";
import { Redirect } from "react-router-dom";
import NavBar from "../components/NavBar"
import signup from "../img/signup.jpg";
import "../css/signup.css"
import Footer from "../components/Footer"


export default function Signup() {

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const [isActive, setIsActive] = useState(false)
    const [willRedirect, setWillRedirect] = useState(false)

    useEffect(() => {
        if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password && confirmPassword) && (mobileNo.length === 11)){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, email, mobileNo, password, confirmPassword])

    // console.log(firstName)
    // console.log(lastName);
    // console.log(email);
    // console.log(mobileNo);
    // console.log(password);
    // console.log(confirmPassword);

    function signupUser(event) {
      event.preventDefault() 

      fetch("https://pure-caverns-71461.herokuapp.com/api/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobileNo: mobileNo,
          password: password,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          if (data.message) {
            Swal.fire({
              icon: "error",
              title: "Registration Failed.",
              text: data.message,
            });
          } else {
            Swal.fire({
              icon: "success",
              title: "Registration Successful!",
              text: "Thank you for registering.",
            });
            setWillRedirect(true);
          }
        });
      setFirstName("");
      setLastName("");
      setMobileNo("");
      setEmail("");
      setPassword("");
      setConfirmPassword("");
    }

    return willRedirect ? (
      <Redirect to="/login" />
    ) : (
      <>
        <NavBar/>
        <Container className="signup-card">
          <Row>
            <Col lg={7} sm={12}>
              <Form onSubmit={(e) => signupUser(e)} className="pt-3 pl-4 pr-3">
                <Form.Group>
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="text"
                    placeholder="Enter your first name"
                    value={firstName}
                    onChange={(e) => {
                      setFirstName(e.target.value);
                    }}
                  ></Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="text"
                    placeholder="Enter your last name"
                    value={lastName}
                    onChange={(e) => {
                      setLastName(e.target.value);
                    }}
                  ></Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="email"
                    placeholder="Enter your email"
                    value={email}
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                  ></Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Mobile Number</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="number"
                    placeholder="Enter 11 digit mobile number"
                    value={mobileNo}
                    onChange={(e) => {
                      setMobileNo(e.target.value);
                    }}
                  ></Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  ></Form.Control>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Confirm Password</Form.Label>
                  <Form.Control
                    className="pt-0"
                    type="password"
                    placeholder="Confirm your password"
                    value={confirmPassword}
                    onChange={(e) => {
                      setConfirmPassword(e.target.value);
                    }}
                  ></Form.Control>
                </Form.Group>
                <Row>
                  <Col>
                    {isActive ? (
                      <Button
                        className="signup-btn"
                        variant="dark"
                        type="submit"
                      >
                        Submit
                      </Button>
                    ) : (
                      <Button
                        className="signup-btn"
                        disabled
                        variant="dark"
                        type="submit"
                      >
                        Submit
                      </Button>
                    )}
                  </Col>
                  <Col>
                    <p className="form-parag">
                      Go Back to <a href="/">Home</a>
                    </p>
                  </Col>
                </Row>
              </Form>
            </Col>
            <Col lg={5} sm={12} className="p-0 order-first">
              <Image src={signup} fluid />
            </Col>
          </Row>
        </Container>
        <Footer/>
      </>
    );
}