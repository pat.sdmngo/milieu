import React, {useState, useEffect} from 'react'
import NavBar from "../components/NavBar"
import Footer from "../components/Footer"
import { Container, Form, Row, Col, Image, Button } from "react-bootstrap";
import shipping from "../img/shipping-banner.jpg"
import Swal from "sweetalert2";
import "../css/signup.css"


export default function ShippingDetails() {

  const [shippingAddress, setShippingAddress] = useState("")
  const [isActive, setIsActive] = useState(false);
  const [order, setOrder] = useState([])

  function handleShippingAddress(event) {
    setShippingAddress(event.target.value)
  }

  useEffect(() => {
    if (shippingAddress !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  });

  function placeOrder(event) {
    event.preventDefault()

    fetch("https://pure-caverns-71461.herokuapp.com/api/orders/cart/checkout", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        confirmation: true,
        shippingAddress: shippingAddress,
      }),
    })
      .then((res) => res.json()).catch(err => console.error(err))
      .then((data) => {
        setOrder(data);
      })
      .catch(err => console.error(err))

    Swal.fire("Your order has been placed!", "Thank you for shopping with us!", "success");
    //window.location.replace("/orders");

    setShippingAddress("")
  }


  return (
    <>
      <NavBar />
      <div className="shipping-page">
        <Container className="shipping-details-card">
          <Row>
            <Col lg={3}>
              <Image src={shipping} fluid="true" />
            </Col>
            <Col lg={9}>
              <Form className="p-3">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Shipping Address</Form.Label>
                  <Form.Control
                    onChange={handleShippingAddress}
                    value={shippingAddress}
                    type="text"
                    placeholder="Enter your address here"
                  />
                  <Form.Text className="text-muted">
                    We apologize in advance for any delays on the scheduled
                    delivery date for areas affected by the ongoing General
                    Community Quarantine. Please bear with us.
                  </Form.Text>
                </Form.Group>
              </Form>
              <Container>
                <Row>
                  <Col>
                    <Button
                      href="/cart"
                      className="shipping-btn"
                      variant="outline-dark"
                    >
                      Go Back to Cart
                    </Button>
                  </Col>
                  <Col className="place-order-container">
                    {isActive ? (
                      <Button onClick={placeOrder} className="shipping-btn" variant="danger">
                        Place Order
                      </Button>
                    ) : (
                      <Button disabled className="shipping-btn" variant="danger">
                        Place Order
                      </Button>
                    )}
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer />
    </>
  );
}