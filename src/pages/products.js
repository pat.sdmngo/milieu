import React, { useEffect, useState, useContext } from "react";
import { Redirect } from "react-router-dom";
import UserContext from "../userContext";
import { Table, Button, Container } from "react-bootstrap";
import NavBar from "../components/NavBar"
import Footer from "../components/Footer"

export default function Products() {

  const [allProducts, setAllProducts] = useState([])
  const [activeProducts, setActiveProducts] = useState([])
  const [willRedirect, setWillRedirect] = useState(false)
  const {user, setUser} = useContext(UserContext)
  const [update, setUpdate] = useState("")

  console.log(user.isAdmin)

  useEffect(() => {
    fetch("https://pure-caverns-71461.herokuapp.com/api/products", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setAllProducts(data);
        let productArray = data;

        let tempArray = productArray.filter((product) => {
          return product.isActive === true;
        });

        setActiveProducts(tempArray);
      });
  }, [update])


  function archive(productId) {
    fetch(
      `https://pure-caverns-71461.herokuapp.com/api/products/archive/${productId}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setUpdate({});
      });
  }

  function active(productId) {
    fetch(
      `https://pure-caverns-71461.herokuapp.com/api/products/activate/${productId}`,
      {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setUpdate({});
      });
  }
  

  let productRows = allProducts.map(product => {
    return (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.isActive ? "Active" : "Inactive"}</td>
        <td>
          {product.isActive ? (
            <Button
              variant="outline-dark"
              className="mx-2 dash-btn"
              onClick={() => archive(product._id)}
            >
              Archive
            </Button>
          ) : (
            <Button
              variant="danger"
              className="mx-2 dash-btn"
              onClick={() => active(product._id)}
            >
              Activate
            </Button>
          )}
        </td>
      </tr>
    );
  })

  return (
    !user.isAdmin || !user.email
    ? 
    <Redirect to="/shop"/>
    :
    <>
      <NavBar />
      <Container>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th className="dashboard-label">ID</th>
              <th className="dashboard-label">Name</th>
              <th className="dashboard-label">Status</th>
              <th className="dashboard-label">Actions</th>
            </tr>
          </thead>
          <tbody className="dashboard-text">{productRows}</tbody>
        </Table>
      </Container>
      <Footer />
    </>
  );
}
