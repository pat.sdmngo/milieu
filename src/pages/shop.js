import React, {useState, useEffect} from 'react'

import { Container, Col, Row, Image } from "react-bootstrap";

import NavBar from "../components/NavBar";
import Product from "../components/Product"
import Banner from "../components/Banner"
import "../css/product.css"
import Footer from "../components/Footer"
import brand1 from "../img/brands/brand-1.PNG"
import brand2 from "../img/brands/brand-2.PNG";
import brand3 from "../img/brands/brand-3.PNG";
import brand4 from "../img/brands/brand-4.PNG";
import brand5 from "../img/brands/brand-5.PNG";
import brand6 from "../img/brands/brand-6.PNG";
import brand7 from "../img/brands/brand-7.PNG";
import brand8 from "../img/brands/brand-8.PNG";
import cartIcon from "../img/shopping-cart.png";





export default function Shop() {

  const [activeProducts, setActiveProducts] = useState([]);


  useEffect(() => {
    fetch("https://pure-caverns-71461.herokuapp.com/api/products/active", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setActiveProducts(data);
      });
  }, []);


  let active = activeProducts.map((product) => {
    return (
      <Col lg={4} sm={12} className="pt-4">
        <Product key={product._id} prodProp={product} />
      </Col>
    );
  });

  return (
    <>
      <NavBar />
      <Banner />
      <div className="shop-heading">
        <h3 className="shop-heading-text">
          Choose from a wide range of high-quality products from our trusted
          brands.
        </h3>
        <Container>
          <Row className="pt-5">
            <Col>
              <Image src={brand1} fluid />
            </Col>
            <Col>
              <Image src={brand2} fluid />
            </Col>
            <Col>
              <Image src={brand3} fluid />
            </Col>
            <Col>
              <Image src={brand4} fluid />
            </Col>
            <Col>
              <Image src={brand5} fluid />
            </Col>
            <Col>
              <Image src={brand6} fluid />
            </Col>
            <Col>
              <Image src={brand7} fluid />
            </Col>
            <Col>
              <Image src={brand8} fluid />
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="mb-5">
        <Row>{active}</Row>
      </Container>
      <div className="hover-cart-btn">
        <a href="/cart">
          <Image src={cartIcon} fluid />
        </a>
      </div>
      <Footer />
    </>
  );
}